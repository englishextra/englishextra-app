# englishextra-app

*English Grammar for Russian-Speakers—an App for Windows / Android / Linux*

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/7c948a05225f4a589590ade4a1aee11f)](https://www.codacy.com/app/englishextra/englishextra-app?utm_source=github.com&utm_medium=referral&utm_content=englishextra/englishextra-app&utm_campaign=badger)
[![Travis](https://img.shields.io/travis/englishextra/englishextra-app.svg)](https://github.com/englishextra/englishextra-app)
[![SourceForge](https://img.shields.io/sourceforge/dm/englishextra-app.svg)](https://sourceforge.net/projects/englishextra-app/)

## Dashboard

<https://build.phonegap.com/apps/1824701/builds>

## Production Push URL

```
https://github.com/englishextra/englishextra-app.git
```

## Remotes

- [GitHub](https://github.com/englishextra/englishextra-app)
- [BitBucket](https://bitbucket.org/englishextra/englishextra-app)
- [GitLab](https://gitlab.com/englishextra/englishextra-app)
- [CodePlex](https://englishextraapp.codeplex.com/SourceControl/latest)
- [SourceForge](https://sourceforge.net/p/englishextra-app/code/)

## Public Download Pages

- [CodePlex](https://englishextraapp.codeplex.com/releases/)
- [SourceForge](https://sourceforge.net/projects/englishextra-app/files/)
- [Bd](https://build.phonegap.com/apps/1824701/share)

## Copyright

© [github.com/englishextra](https://github.com/englishextra), 2015-2018